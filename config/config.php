<?php

/**
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Development environments
 */
const ENVIRONMENT_PRODUCTION = 'production';
const ENVIRONMENT_DEVELOPMENT = 'development';
const ENVIRONMENT_MAINTENANCE = 'maintenance';

define ('ENVIRONMENT', ENVIRONMENT_DEVELOPMENT);
/**
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */

$aModules = array('frontend');//, 'admin');

return new \Phalcon\Config(array(
    'database' => array(
        'adapter' => 'Mysql',
        'host' => 'localhost',
        'username' => 'root',
        'password' => '',
        'dbname' => 'phalcon_portfolio',
    ),
    'application' => array(
        'controllersDir' => __DIR__ . '/../apps/controllers/',
        'modelsDir' => __DIR__ . '/../apps/models/',
        'viewsDir' => __DIR__ . '/../apps/views/',
        'viewsDir_frontend' => __DIR__ . '/../apps/frontent/views/',
        'viewsDir_admin' => __DIR__ . '/../apps/admin/views/',
        'pluginsDir' => __DIR__ . '/../apps/plugins/',
        'libraryDir' => __DIR__ . '/../apps/library/',
        'cacheDir' => __DIR__ . '/../apps/cache/',
        'logDir' => __DIR__ . '/../logs/logger/',
        'picturesDir' => __DIR__ . '/../public/cdn/images/',
        'baseUri' => '/',
        'siteUrl' => 'http://phalcon/',
        'siteName' => 'My Site Name'
    )
));
