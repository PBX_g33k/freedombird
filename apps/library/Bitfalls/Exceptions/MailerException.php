<?php

namespace Bitfalls\Exceptions;

/**
 * Class MailerException
 * @package Bitfalls\Mailer
 */
class MailerException extends \SendCloud\Exceptions\BaseException
{

}