<?php

namespace G33ksoft\Tools;

use G33ksoft\Tools\Cookie;

Class CookieCollection
{
	protected $cookies = array();

	public function __construct() { }

	public function addSetCookie($setCookieString) {
		// Parse Set-Cookie string
		$cookie = $this->parseCookie($setCookieString);
		//$this->cookies[$cookie->getName()] = $cookie;
		$this->addCookie($cookie);
	}

	public function addCookie(Cookie $cookie) {
		$this->cookies[$cookie->getName()] = $cookie;
	}

	public function GetByName($name) {
		if(isset($cookies[$name])) {
			return $cookies[$name];
		} else {
			return false;
		}
	}

	public static function parseCookie($cookieString) {
		$cookie = new Cookie();
		$pairs = explode(';', trim($cookieString));
		foreach($pairs as $pair) {
			$KeyValue = explode('=', trim($pair), 2);
			if(count($KeyValue) == 2) {
				$val = trim($KeyValue[1]);
				switch(strtolower($KeyValue[0])) {
					case 'path':
						$cookie->setPath($val);
						break;
					case 'domain':
						$cookie->setDomain($val);
						break;
					case 'expires':
						$cookie->setExpires(new \DateTime($val));

						break;
					default:
						$cookie->setName($KeyValue[0]);
						$cookie->setValue($val);
						break;
				}
			} else if(!$KeyValue) {
				switch(strtolower($pair)) {
					case 'secure':
						$cookie->setSecure(TRUE);
						break;
					case 'httponly':
						$Cookie->setHTTPOnly(TRUE);
						break;
				}
			}
		}
		return $cookie;
	}
}