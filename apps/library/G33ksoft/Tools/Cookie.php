<?php

namespace G33ksoft\Tools;

class Cookie
{
	protected $name;
	protected $value;
	protected $domain;
	protected $path = '/';
	protected $expires = null;
	protected $Secure = false;
	protected $HTTPOnly = false;

	public function __construct() { }

	public function setName($name) {
		$this->name = $name;
		return $this;
	}

	public function setValue($value) {
		$this->value = $value;
		return $this;
	}

	public function setDomain($domain) {
		$this->domain = $domain;
		return $this;
	}

	public function setPath($path) {
		$this->path = $path;
		return $this;
	}

	public function setExpires(\DateTime $expires) {
		$this->expires = $expires;
		return $this;
	}

	public function setSecure($Secure) {
		$this->Secure = $Secure;
		return $this;
	}

	public function setHTTPOnly($HTTPOnly) {
		$this->HTTPOnly = $HTTPOnly;
		return $this;
	}

	public function getName() {
		return $this->name;
	}

	public function getValue() {
		return $this->value;
	}

	public function getDomain() {
		return $this->domain;
	}

	public function getPath() {
		return $this->path;
	}

	public function getExpires() {
		return $this->expires;
	}

	public function getSecure() {
		return $this->Secure;
	}

	public function getHTTPOnly() {
		return $this->HTTPOnly;
	}
}