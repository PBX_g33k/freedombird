<?php

namespace G33ksoft\Proxy;

use Phalconlib\Http\Client\Response;
use Phalconlib\Http\Uri;
use PHPHtmlParser\Dom;

class ProxyBase
{
	protected $baseUrl = null;
	public $header = null;
	protected $uri = null;
	protected $cookies = null;
	protected $lastResponse = null;
	protected $responseAssets = null; // Contains all assets that are parsed from response
	protected $dom = null;
	private $provider;

	const VERSION = '0.0.1';

	public function __construct() {}

	public function setBaseUrl($baseUrl) {
		$this->baseUrl = $baseUrl;
	}

	public function getBaseUrl() {
		return $this->baseUrl;
	}

	public function setHeader($key, $value) {
		$this->header[$key] = $value;
	}

	public function getHeader($key) {
		return $this->header[$key];
	}

	public function setHeaders($headers) {
		foreach($headers as $key => $value) {
			$this->header[$key] = $value;
		}
	}

	public function getHeaders() {
		return $this->header;
	}

	public function setUri($uri) {
		$this->uri = $uri;
	}

	public function getUri() {
		return $this->uri;
	}

	public function requestCachedUri($uri) {
		$frontCache = new \Phalcon\Cache\Frontend\Output(array("lifetime" => 1));
		$cache = new \Phalcon\Cache\Backend\File($frontCache, array("cacheDir" => __DIR__.'/../../../../cache/'));

		$cacheFilename = str_replace('/', '_', str_replace('.', '__', $uri)).'.cache';
		$content = $cache->get($cacheFilename);

		if($content === null) {
			$content = $this->requestUri($uri);
			$cache->save($cacheFilename, $content);
		}

		var_dump($content,$cache);die();

		return $content;
	}

	public function requestUri($uri) {
		$this->buildProvider();
		$this->lastResponse = $this->provider->get($uri);
		$this->processResponse();
		return $this->lastResponse;
	}

	public function request() {
		return $this->requestUri($this->uri);
	}

	private function buildProvider() {
		$this->provider = ProxyRequest::getProvider();
		$this->provider->setBaseUri($this->baseUrl);

		if(!is_array($this->header) || !is_object($this->header))
			return;

		foreach($this->header as $key => $value) {
			$this->provider->header->set($key,$value);
		}
	}

	private function processResponse() {
		// Set cookies
		$cookies = $this->lastResponse->header->getCookies();
		if(!is_array($cookies))
			return;

		foreach($cookies as $cookieKey => $cookieValue) {
			$this->cookies[$cookieKey] = $cookieValue;
		}

		// Rewrite Uri's
		// $this->parseBody($this->lastResponse->body);
		// $this->parseHtmlUsingXPath($this->lastResponse->body);
	}

	private function parseHtmlUsingXPath($responseBody) {
		/* Use internal libxml errors -- turn on in production, off for debugging */
		libxml_use_internal_errors(true);
		/* Createa a new DomDocument object */
		$dom = new \DomDocument;
		/* Load the HTML */
		$dom->loadHTML($responseBody);
		/* Create a new XPath object */
		$xpath = new \DomXPath($dom);
		/* Query all <td> nodes containing specified class name */
		$nodes = $xpath->query("//td[@class='topicViews']");
		var_dump($dom);
		$this->searchHeadAssets($xpath);
	}

	private function parseBody($body) {
		// Look for all assets in the body
		$this->dom = new Dom();
		$this->dom->load($body);
		$this->searchAssets();
	}

	private function searchHeadAssets(\DomXPath $xpath) {
		$head = $xpath->query("/html/head");
		var_dump($head);die();
	}

	private function searchAssets() {
		var_dump($this->dom);die();
		// look for assets in the header

		// look for CSS assets (to join and minify it, saving bandwith)

		// look for JS assets (to join and minify it, saving bandwith)

		// look for assets in the footer
	}
}