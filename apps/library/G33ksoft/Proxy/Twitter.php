<?php

namespace G33ksoft\Proxy;

class Twitter extends ProxyBase
{
	protected $targetdomain = 'phalcon.portfolio.local';
	public $baseUrl = 'https://www.twitter.com/';
	protected $rewriteUris = array(
		'www.twitter.com' => 'phalcon.portfolio.local',
		'twitter.com' => 'phalcon.portfolio.local',
		'abs.twimg.com' => 'phalcon.portfolio.local',
		'pbs.twimg.com' => 'phalcon.portfolio.local',
		'https://' => 'http://'
	);

	const VERSION = '0.0.1';

	public function __construct() {}

	public function getIndex() {
		//$this->setIndexHeaders();
		$response = $this->requestUri('/');
		// $response = $this->requestCachedUri('/');
		return $this->simpleMassReplace($response->body);
	}

	public function getCachedFromUrlFromArgs($args) {
		$uri = '';
		for ($i=0; $i < count($args); $i++) { 
			if($i > 0)
				$uri .= '/';
			$uri .= $args[$i];
		}
		$this->setIndexHeaders();
		return $this->requestUri($uri);
		// return $this->requestCachedUri($uri);
	}

	public function getFromUrlFromArgs($args) {
		$uri = '';
		for ($i=0; $i < count($args); $i++) { 
			if($i > 0)
				$uri .= '/';
			$uri .= $args[$i];
		}
		$this->setIndexHeaders();
		return $this->requestUri($uri);
	}

	private function setIndexHeaders() {
		$this->setHeaders(
			array(
				'Accept' => 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
		    	'Cache-Control' => 'no-cache',
		    	'Pragma' => 'no-cache',
			)
		);
	}

	private function clearHeaders() {
		$this->header = null;
	}

	private function loadAssets() {
		// Look for external assets to proxy (and cache) it
	}

	private function simpleMassReplace($body) {
		foreach($this->rewriteUris as $search => $replace) {
			$body = $this->simpleReplace($search, $replace, $body);
		}
		return $body;
	}

	private function simpleReplace($search, $replace, $responseBody) {
		return str_replace($search, $replace, $responseBody);
	}
}