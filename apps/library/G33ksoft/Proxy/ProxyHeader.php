<?php

namespace G33ksoft\Proxy;

use Phalconlib\Http\Client\Header;
use G33ksoft\Tools\CookieCollection;
use G33ksoft\Tools\Cookie;

class ProxyHeader extends Header {
    protected $cookies;

	public function __construct() {
        //$this->cookies = new CookieCollection();
    }

    public function getCookie($name) {
        return $this->cookies->name;
    }

    public function getCookies() {
        return $this->cookies;
    }

	public function parse($content)
    {
        if (empty($content)) {
            return false;
        }

        if (is_string($content)) {
            $content = array_filter(explode("\r\n", $content));
        } elseif (!is_array($content)) {
            return false;
        }

        $status = array();
        if (preg_match('/^HTTP\/(\d(?:\.\d)?)\s+(\d{3})\s+(.+)$/i', $content[0], $status)) {
            $this->status = array_shift($content);
            $this->version = $status[1];
            $this->statusCode = intval($status[2]);
            $this->statusMessage = $status[3];
        }

        foreach ($content as $field) {
            if (!is_array($field)) {
                $field = array_map('trim', explode(':', $field, 2));
            }

            if (count($field) == 2) {
                $this->set($field[0], $field[1]);
                if(strtolower($field[0]) == 'set-cookie') {
                    //$this->cookies->addSetCookie($field[1]);
                    $cookie = CookieCollection::parseCookie($field[1]);
                    $this->cookies[$cookie->getName()] = $cookie;
                }
            }
        }

        return true;
    }
}