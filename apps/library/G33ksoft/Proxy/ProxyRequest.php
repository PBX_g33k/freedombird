<?php

namespace G33ksoft\Proxy;

use G33ksoft\Proxy\Provider\Curl;
use G33ksoft\Proxy\Provider\Exception as ProviderException;
use G33ksoft\Proxy\Provider\Stream;
use Phalconlib\Http\Uri;

class ProxyRequest
{
    protected $baseUri;
    public $header = null;

    const VERSION = '0.0.1';
    
	public function __construct() {
		$this->baseUri = new Uri();
		$this->header = new ProxyHeader();
	}

    public static function getProvider()
    {
        if (Curl::isAvailable()) {
            return new Curl();
        }

        if (Stream::isAvailable()) {
            return new Stream();
        }

        throw new ProviderException('There isn\'t any available provider');
    }

    public function setBaseUri($baseUri)
    {
        $this->baseUri = new Uri($baseUri);
    }

    public function getBaseUri()
    {
        return $this->baseUri->toString();
    }

    public function resolveUri($uri)
    {
        return $this->baseUri->resolve($uri);
    }

}