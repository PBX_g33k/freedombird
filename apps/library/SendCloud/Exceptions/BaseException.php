<?php

namespace SendCloud\Exceptions;
use Phalcon\Crypt as Crypt;

/**
 * Class BaseException
 * @package SendCloud\Exceptions
 * @author Oguzhan Uysal
 * @version 0.1.0
 */
class BaseException extends \Exception {

	protected $key = 'SendCloudDebug_error_(^*&YFO';
	private $crypt;

	public function __construct($message = null, $code = 0, Exception $previous = null) {
		// Initialize Crypt handler
		$this->crypt = new Crypt();
		parent::__construct();
	}

	/**
	 * Returns the encrypted Exception. Not advised to print to browser
	 * @return string Encrypted exception presented as a string
	 */
	public function getEncryptedException() {
		// Serialize this Exception Object
		$serialized_object = serialize($this);
		return $this->crypt->encrypt($serialized_object, $this->key);
	}

	/**
	 * Returns the base64 encoded AND encrypted Exception, this method is advised if
	 * the exception will be shown to the user or send to backend.
	 * @return string Encrypted exception presented as a base64 encoded string
	 */
	public function getEncryptedBase64Exception() {
		// Serialize this Exception Object
		$serialized_object = serialize($this);
		return $this->crypt->encryptBase64($serialized_object, $this->key);
	}

	/**
	 * Sends the complete Exception (encrypted) to the support vendor.
	 * 
	 * This is a proxy function, if vendor changes update BaseException
	 * @return void
	 */
	public function sendToSupport() {
		$this->sendToZenDesk();
	}

	/**
	 * Sends the complete Exception to ZenDesk API
	 * @return void
	 * @todo Hook and use ZenDesk API
	 */
	private function sendToZenDesk() {
		$payload = $this->getEncryptedBase64Exception();
		$title = sprintf("[SYS][EXCEPTION][CODE: %s][MESSAGE: %s", $this->code, $this->message);
	}

	/**
	 * Stores the complete Exception (encrypted) to a flat file somewhere on the server.
	 * Path is configured in the global configuration(!)
	 * @return void
	 * @todo Write logic to write to file
	 */
	public function sendToFile() {

	}
}