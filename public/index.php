<?php

use Phalcon\Mvc\Application;

error_reporting(E_ALL);

// try {

    /**
     * Read the configuration
     */
    $config = include __DIR__ . '/../config/config.php';
    
    /**
     * Read auto-loader
     */
    include __DIR__ . "/../config/loader.php";

    /**
     * Include services
     */
    require __DIR__ . '/../config/services.php';

    /**
     * Handle the request
     */
    $application = new Application();

    /**
     * Assign the DI
     */
    $application->setDI($di);

    /**
     * Include modules
     */
    require __DIR__ . '/../config/modules.php';

    echo $application->handle()->getContent();

/*} catch (Phalcon\Exception $e) {
    echo $e->getMessage();
} catch (PDOException $e) {
    echo $e->getMessage();
}
*/